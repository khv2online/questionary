/* Russian (UTF-8) initialisation for the jQuery UI date picker plugin. */
/* Written by Andrew Stromnov (stromnov@gmail.com). */
var IS_PRODUCTION = 1;

(function(factory) {
    if (typeof define === "function" && define.amd) {

        // AMD. Register as an anonymous module.
        define(["../widgets/datepicker"], factory);
    } else {

        // Browser globals
        factory(jQuery.datepicker);
    }
}(function(datepicker) {

    datepicker.regional.ru = {
        closeText: "Закрыть",
        prevText: "&#x3C;Пред",
        nextText: "След&#x3E;",
        currentText: "Сегодня",
        monthNames: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь",
            "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"
        ],
        monthNamesShort: ["Янв", "Фев", "Мар", "Апр", "Май", "Июн",
            "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек"
        ],
        dayNames: ["воскресенье", "понедельник", "вторник", "среда", "четверг", "пятница", "суббота"],
        dayNamesShort: ["вск", "пнд", "втр", "срд", "чтв", "птн", "сбт"],
        dayNamesMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
        weekHeader: "Нед",
        dateFormat: "dd.mm.yy",
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ""
    };
    datepicker.setDefaults(datepicker.regional.ru);

    return datepicker.regional.ru;

}));
(function(factory) {
    if (typeof define === "function" && define.amd) {
        define(["jquery", "../jquery.validate"], factory);
    } else if (typeof module === "object" && module.exports) {
        module.exports = factory(require("jquery"));
    } else {
        factory(jQuery);
    }
}(function($) {

    $.extend($.validator.messages, {
        required: "Это поле необходимо заполнить.",
        remote: "Пожалуйста, введите правильное значение.",
        email: "Пожалуйста, введите корректный адрес электронной почты.",
        url: "Пожалуйста, введите корректный URL.",
        date: "Пожалуйста, введите корректную дату.",
        dateISO: "Пожалуйста, введите корректную дату в формате ISO.",
        number: "Пожалуйста, введите число.",
        digits: "Пожалуйста, вводите только цифры.",
        ccard: "Пожалуйста, введите правильный номер кредитной карты.",
        equalTo: "Пожалуйста, введите такое же значение ещё раз.",
        extension: "Пожалуйста, выберите файл с правильным расширением.",
        maxlength: $.validator.format("Пожалуйста, введите не больше {0} символов."),
        minlength: $.validator.format("Пожалуйста, введите не меньше {0} символов."),
        rangelength: $.validator.format("Пожалуйста, введите значение длиной от {0} до {1} символов."),
        range: $.validator.format("Пожалуйста, введите число от {0} до {1}."),
        max: $.validator.format("Пожалуйста, введите число, меньшее или равное {0}."),
        min: $.validator.format("Пожалуйста, введите число, большее или равное {0}."),
    });

}));

$.validator.addMethod("summRange", function(value, element, ranges) {
    value = parseInt(value.replace(/\D/g, ""));
    return ((value >= ranges[0]) && (value <= ranges[1]));
}, "Проверьте правильность суммы!");

$.validator.addMethod(
    "regex",
    function(value, element, regexp) {
        var re = new RegExp(regexp);
        return this.optional(element) || re.test(value);
    },
    "Поле заполнено не верно!"
);

$.validator.addMethod("customdate",
    function(value /*, element*/ ) {
        var isProper = value.match(/^(0?[1-9]|[12][0-9]|3[0-1])[/.](0?[1-9]|1[0-2])[/.](19|20)?\d{2}$/);

        if (!isProper) { 
            return false;
        } else {
            var datePeriods = value.split(".");
            var selectedDate = new Date(datePeriods[2],datePeriods[1]-1,datePeriods[0]);
            var currentDate = new Date();
            if (selectedDate > currentDate) {
                return false;
            }else{
                return true;
            }
        }
    },
    "Пожалуйста, введите корректную дату."
);



function PagePreloader(el) {
    var self = this;
    self.isActive = true;
    self.el = el;
    self.show = function() {
        if (self.isActive) return false;
        self.el.classList.add("b-preloader--active");
        self.isActive = true;
    };

    self.hide = function() {
        if (!self.isActive) return false;
        self.el.classList.remove("b-preloader--active");
        self.isActive = false;
    };
}

function modifyDatepickerHeaderLayout(inst) {
    //console.log(inst);
    window.requestAnimationFrame(function() {

        var $widgetHeader = $(inst).datepicker("widget").find(".ui-datepicker-header:first");
        var $input = $(inst.input);

        var $prevYrBtn = $('<button class="ui-datepicker-prev-year-arrow"></button>');
        $prevYrBtn.on("click", function() {
            $.datepicker._adjustDate($input, -1, 'Y');

        });
        var $nextYrBtn = $('<button class="ui-datepicker-next-year-arrow"></button>');
        $nextYrBtn.on("click", function() {
            $.datepicker._adjustDate($input, +1, 'Y');

        });

        var $monthBlock = $widgetHeader.find('.ui-datepicker-month:first');
        var $prevMonthBtn = $widgetHeader.find('.ui-datepicker-prev:first');
        var $nextMonthBtn = $widgetHeader.find('.ui-datepicker-next:first');

        if ($prevMonthBtn.hasClass('ui-state-disabled')) {
            $prevYrBtn.addClass('ui-state-disabled');
        }

        if ($nextMonthBtn.hasClass('ui-state-disabled')) {
            $nextYrBtn.addClass('ui-state-disabled');
        }

        var $yearBlock = $widgetHeader.find('.ui-datepicker-year:first');
        $prevYrBtn.appendTo($yearBlock);
        $nextYrBtn.prependTo($yearBlock);

        $prevMonthBtn.html("").prependTo($monthBlock);
        $nextMonthBtn.html("").appendTo($monthBlock);
    });
}

function enableUIformControls(el) {
    var $el = $(el);
    $el.find("select").selectmenu().on("selectmenuchange", function(event, ui) {
        $(event.target).valid();
    });
    $el.find(".checkbox input, .radio input").checkboxradio();
    $el.find(".datepicker").each(function(index, el) {
        $(el).datepicker({
            beforeShow: function(input, inst) {
                modifyDatepickerHeaderLayout(inst);
            },
            onChangeMonthYear: function(year, month, inst) {
                modifyDatepickerHeaderLayout(inst);
            },
            maxDate: new Date(),
            //minDate: "-100y -1w"
            defaultDate: el.dataset.defaultDate || "+0",
        }).on('change', function() {
            $(this).valid();

        });

    });

    var $inputs = $(el).find('input');

    $inputs.not("[data-inputformat]").inputmask({ showMaskOnHover: false });

    $inputs.filter("[data-inputformat='num']").each(function(index, el) {
        new Cleave(el, {
            numeral: true,
            numeralThousandsGroupStyle: 'thousand',
            delimiter: ' ',
            numeralPositiveOnly: true,

        });
    });
}

function addAnotherFormBlock(btn) {
    var $btn = $(btn);
    var $template = $($btn.data("fieldset-clone"));

    var currenCloneNum = parseInt($template.data("next-item-num"));

    if (!currenCloneNum || currenCloneNum <= 0) {
        currenCloneNum = 1;
    }



    var $newBlock = $($template.html());
    $newBlock.find('.clonedCardNum').html(currenCloneNum);
    $newBlock.find('select,input').each(function(index, el) {
        el.name += currenCloneNum;
    }).rules();
    $newBlock.css("opacity", 0);
    $newBlock.find('.del_card:first').on('click', function(event) {
        event.preventDefault();
        $newBlock.remove();
        $template.data("next-item-num", $template.data("next-item-num") - 1);
        $btn.parent().show();
    });

    $template.before($newBlock);
    $template.data("next-item-num", currenCloneNum + 1);
    enableUIformControls($newBlock);
    $newBlock.css("opacity", 1);

    var maxClonesNum = $template.data('max-clones');

    if (maxClonesNum && (maxClonesNum <= currenCloneNum)) {
        $btn.parent().hide();
    }
}


window.pagePreloader = new PagePreloader(document.getElementById('pagePreloader'));
enableUIformControls(document.body);
$("#residentialSelect").on("selectmenuchange", function(event, ui) {
    window.pagePreloader.show();
    window.requestAnimationFrame(function() {
        $("#firstStepInitialyDisabledBlocks").addClass('isDisabled');
        $("#step2btn").addClass('isDisabled');
        $("#banksButtons").empty();
    });

    $.ajax({
            url: 'ajax-examples/get-buildings-list/' + ui.item.value + '.json',
            dataType: 'json',
        })
        .done(function(data) {
            var buildingsList = data;
            var options = [];
            (function() {
                var firstOption = document.createElement("option");
                firstOption.value = "";
                firstOption.innerHTML = "-- Выберите дом --";
                firstOption.disabled = "disabled";
                firstOption.selected = "selected";
                options.push(firstOption);
            })();
            $.each(buildingsList, function(index, building) {
                var option = document.createElement("option");
                option.value = building.id;
                option.innerHTML = building.num;
                options.push(option);
            });

            $("#buidingsNumSelect")
                .empty()
                .append(options)
                .selectmenu("refresh");

            $("#buildingNumSelectBlock").removeClass('isDisabled');

        })
        .fail(function() {
            alert("Невозможно получить данные о номерах домов!\nОбратитесь к администратору сайта.");
        }).always(function() {
            window.pagePreloader.hide();
        });
});

function updateBanksList(cb) {
    var earnID = $("#earnMethodSelect").val();
    var bldID = $("#buidingsNumSelect").val();
    var initialFee = $("#initialFeeInp").val();
    var creditTerm = $("#creditTermInp").val();
    var dob = $("#DOBinp").val();
    if (bldID && earnID && initialFee && creditTerm && dob) {
        $.ajax({
                url: 'ajax-examples/banks.json',
                dataType: 'json',
                data: {
                    earnid: earnID,
                    bldid: bldID,
                    initialfee: initialFee,
                    creditterm: creditTerm,
                    dob: dob,

                },
            })
            .done(function(data) {
                var banks = data;

                if (banks && banks.length) {
                    var banksBtns = [];
                    for (var i = banks.length - 1; i >= 0; i--) {
                        var label = document.createElement("label");

                        var logo = document.createElement("img");
                        logo.src = banks[i].logo;
                        label.appendChild(logo);

                        var input = document.createElement("input");
                        input.type = "checkbox";
                        input.checked = "checked";
                        input.name = banks[i].id;
                        label.appendChild(input);
                        banksBtns.push(label);
                    }
                    window.requestAnimationFrame(function() {

                        $("#banksButtons").empty().append(banksBtns).find("input").checkboxradio();
                        $("#step2btn").removeClass('isDisabled');
                    });
                } else {
                    $("#banksButtons").empty();
                    $("#step2btn").addClass('isDisabled');
                    //alert("Нет банков удовлетворяющих условиям.");
                }
            })
            .fail(function() {
                alert("Невозможно получить данные о банках!\nОбратитесь к администратору сайта.");
            })
            .always(function() {
                if (cb) cb();
            });
    } else {
        if (cb) cb();
    }
}


$("#buidingsNumSelect, #earnMethodSelect").on('selectmenuchange', function(event, ui) {
    event.preventDefault();
    window.pagePreloader.show();

    updateBanksList(function() {
        window.pagePreloader.hide();
    });

    if (event.target.id === "buidingsNumSelect") {
        $("#firstStepInitialyDisabledBlocks").removeClass('isDisabled');
    }
});

$("#DOBinp").on('change', _.debounce(function() {
    updateBanksList();
}, 120));

$("#initialFeeInp, #creditTermInp").on('input', _.debounce(function() {
    updateBanksList();
}, 120));


$(document).on('selectmenuchange', ".jobtypeSelect", function(event, ui) {
    event.preventDefault();
    var $selectedOption = $(ui.item.element[0]);
    var $template = $($selectedOption.data("insert-fieldset"));
    var $cont = $selectedOption.parents(".row_t:first").next(".jobTypeFieldsetContainer:first");

    if ($template.length) {
        var $newBlock = $($template.html());
        $newBlock.css("opacity", 0);


        var currenCloneNum = parseInt($template.data("next-item-num"));

        if (!currenCloneNum || currenCloneNum <= 0) {
            currenCloneNum = 1;
        }
        $newBlock.find('select,input').each(function(index, el) {
            el.name += currenCloneNum;
        }).rules();


        enableUIformControls($newBlock);
        $cont.empty().append($newBlock);
        $template.data("next-item-num", currenCloneNum + 1);
        $newBlock.css("opacity", 1);
    } else {
        $cont.empty();
    }


}).on('change', '[data-toggle-relation-role-field]', function(event) {
    event.preventDefault();
    var $chkb = $(event.currentTarget);
    var $fieldWrap = $chkb.parents(".checkbox:first").next(".row_t");
    var $select = $fieldWrap.find('select:first');
    if ($chkb.is(':checked')) {
        $fieldWrap.show();
        $select.selectmenu("enable");
    } else {
        $fieldWrap.hide();
        $select.selectmenu("disable");
    }
}).on('selectmenuchange', '[data-toggle-line-of-business-field]', function(event) {
    event.preventDefault();
    var $srcSelect = $(event.currentTarget);
    var $descFieldWrap = $srcSelect.parents(".row_t:first").next();
    if ($srcSelect.val() === "Другое") {
        $descFieldWrap.show().find('input:first').removeAttr('disabled');
    } else {
        $descFieldWrap.hide().find('input:first').attr('disabled', 'disabled');
    }

});

$(document.body).on('click', "[data-fieldset-clone]", function(event) {
    event.preventDefault();
    window.requestAnimationFrame(function() {
        addAnotherFormBlock(event.currentTarget);
    });

    return false;
});


function QuestionaryApp() {
    var self = this;
    self.previousStep = 0;
    self.currentStep = ko.observable();
    self.currentStep.subscribe(function() {
        setTimeout(function() {
            window.scrollTo(0, 0);
        }, 100);
    });
    self.hasTemporalRegistration = ko.observable(false);
    self.hasActuallLivingPlace = ko.observable(false);
    self.hasEstate = ko.observable(false);
    self.hasVehicle = ko.observable(false);
    self.hasOriginalNames = ko.observable(false);

    self.step1EstatePrice = ko.observable();
    self.step1InitialFee = ko.observable();
    self.step1MothersCapital = ko.observable();
    self.banksResult = ko.observable(false);

    self.step1CreditSumm = ko.computed(function() {
        var estatePrice = parseInt((self.step1EstatePrice() + "").replace(/\D/g, ""));
        var initFee = parseInt((self.step1InitialFee() + "").replace(/\D/g, "")) || 0;
        var motherCapital = parseInt((self.step1MothersCapital() + "").replace(/\D/g, "")) || 0;
        var n = estatePrice - initFee - motherCapital;
        var res;
        if (n > 0) {
            res = n.toLocaleString("ru-RU");
        } else {
            res = 0;
        }
        return res;
    }, this);



    Sammy(function() {

        this.get('questionary/#step_5', function(opts) {

            $.ajax({
                    url: 'ajax-examples/getPDF.json',
                    dataType: 'json',
                    data: $("#questionaryForm").serialize(),
                })
                .done(function(banksResult) {
                    self.banksResult(banksResult);
                    self.currentStep(5);
                })
                .fail(function() {
                    alert("Не удалось сгенерировать список документов!\nОбратитесь к администратору сайта...");
                })
                .always(function() {
                    console.log("complete");
                });

        });

        this.get('questionary/#step_:stepnum', function(opts) {
            self.banksResult(false);
            var prevStep = self.previousStep;
            var nextStep = parseInt(opts.params.stepnum);

            if ( prevStep === 0 && IS_PRODUCTION) {
                self.previousStep = 1;
                this.app.setLocation("questionary/#step_1");
                self.currentStep(1);

            } else if ( prevStep < nextStep && IS_PRODUCTION) {
                var $currentSection = $("#step_" + prevStep);

                $currentSection.find('input,select').valid();
                var $invalidEl = $currentSection.find('.error:visible:first');
                if ($invalidEl.length) {
                    self.currentStep(parseInt(prevStep));
                    this.app.setLocation("questionary/#step_" + prevStep);
                    $('html, body').animate({
                        scrollTop: $invalidEl.offset().top - 40
                    }, 400);
                } else {
                    self.previousStep = nextStep;
                    self.currentStep(parseInt(nextStep));
                }
            } else {
                self.previousStep = nextStep;
                self.currentStep(parseInt(nextStep));
            }
        });
    }).run("questionary/#step_1");
}

window.questApp = new QuestionaryApp();
ko.applyBindings(window.questApp);

$("#questionaryForm").validate({
    errorPlacement: function(error, element) {
        if (element.attr("type") == "checkbox")
            error.insertAfter(element.parent().siblings().last());
        else if (element.is("select")) {
            error.insertAfter(element.next(".ui-selectmenu-button"));
        } else error.insertAfter(element);
    },
    ignore: [],
    onkeyup: false,
    //focusCleanup: true,
});

$("#yui-main").css('visibility', 'visible');

window.pagePreloader.hide();